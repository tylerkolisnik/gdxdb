# README #

Testers (unix and windows) welcomed!
Feedback welcomed!

### What is this repository for? ###

* gdxdb R package
* v0.1 - preparing for v3.0 PRNDL

### How do I get set up? ###

Install from [bitbucket](https://www.rdocumentation.org/packages/devtools/versions/1.13.3/topics/install_bitbucket)
Clone the repo, build and install from source.

For easy installation:
library(devtools)
install_bitbucket('genomedx/gdxdb')

See vignette for quick start.
Documentations are incomplete. Updates coming soon...

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact