## This is a script for importing RNA batch data to PRNDL. 
## Created by Tyler Kolisnik and Lucia Lam
## Directions: Change the variable rnaBatch to the rnaBatch number you would like to import. Press Run.
## Requirements:
## If full DBRNA file path is not known, the code looks for the file by batch.
## Box has to be mounted with folder structures configures in locations.R, for example:
## /media/box.net/Laboratory Operations/CLIA Laboratory/DocuBox CLS Private Production/

## Load Libraries

options(stringsAsFactors =FALSE)
source("locations.R")
library(xlsx)
library(gdxdb)
library(futile.logger)

#===============================================================================
# USER INPUT
#===============================================================================

## Change this to the RNA Batch number you would like to import
rnaBatch <- NULL #"01386" 

# leave this as null or specify file path if no rna batch available
rnaFileName <- NULL


#===============================================================================
# START
#===============================================================================
# load excel file
#===============================================================================

prndlConnect()

if (is.null(rnaBatch)){
    batchName <- NULL
} else {
    batchName<-paste0('GBX-Batch-',rnaBatch)
}

if(is.null(rnaFileName)){
    if (is.null(rnaBatch)){
        flog.error("Please provide rnaBatch or full rnaFileName")
        stop("Terminating program")
    }
    # Find Excel file in Box.
    # Box must be mounted to /media/box.net and you must have the same folder structure in place.
    batchList <- dir(rnaBoxLocation, full.names=TRUE)
    dirBatchList <- dir(batchList, full.names=TRUE)
    grepNums <- grep(rnaBatch, dirBatchList, value=TRUE)
    grepRNA <- grep("RNA", grepNums, value=TRUE)
    RNAfileList <- dir(grepRNA, full.names=TRUE)
    rnaFileLoc <- grep("DBRNAnanodrop", RNAfileList, ignore.case=TRUE)
    rnaFileName <- RNAfileList[rnaFileLoc]    
}

if (length(rnaFileName) > 1) {
    flog.error("More than one rna file found:\n%s", paste(rnaFileName, collapse="\n"))
}

## Read in Excel File
rnaData <- read.xlsx(rnaFileName, 1)

#===============================================================================
# clean and transform data
#===============================================================================

# trimDF transforms df if one record
rnaData <- trimDF(rnaData) #Clean up leading and trailing whitespace
flog.info("%s records retrieved", nrow(rnaData))

# Subset all where study is not blank
#colnames(RNA.results)[grep("study",names(RNA.results),ignore.case=TRUE)] <- "Study"
rnaExtracted <- subset(rnaData, !is.na(rnaData$Study))
flog.info("%s extracted records", nrow(rnaExtracted))

rnaExcluded <- subset(rnaData, is.na(rnaData$Study))
flog.info("%s records excluded.", nrow(rnaExcluded))

## Get study name
rnaExtracted$study_name <- studyNamebyAccessionCode(rnaExtracted$Study, conn)
flog.info("Processing %s", paste(unique(rnaExtracted$study_name), collapse=","))

## Reformat the data frame 
RNA.results <- rnaExtracted
if (is.null(batchName)){
    if ("rna_batch" %in% colnames(RNA.results)){
        RNA.results$rna_batch <- paste0("GBX-Batch-", RNA.results$rna_batch)
    } else {
        flog.warning("No rna batch provided")
        RNA.results[,"rna_batch"] <- paste0(rnaExtracted$Study, "-Multi-Batch")
    }
} else {
    RNA.results[,"rna_batch"] <- batchName
}

# rename headers to match db
RNA.results <- renameDBRNAHeader(RNA.results)
RNA.results <- transformRNA(RNA.results)

#===============================================================================
# finalize and validate
#===============================================================================

## Subset to keep only the columns that will be imported
## Specify the column names to keep
## TODO: break into required and optional
keeps <- c("rna_well",
           "sample_id",
           "rna_date",
           "rna_concentration",
           "rna_260",
           "rna_280",
           "rna_260_280",
           "rna_260_230",
           "rna_extraction_method",
           "study_name",
           "rna_batch",
           "rna_id",
           "original_rna_id",
           "rna_yield",
           "nucleic_acid_type",
           "rna_volume")

# lenient
#RNA.results <- RNA.results[,(names(RNA.results) %in% keeps)]

# strict
if (all(keeps %in% colnames(RNA.results))){
    RNA.results <- RNA.results[, keeps]

} else {
    flog.error("one or more required column(s) not found")
    flog.error("missing headers:\n%s",
               paste(setdiff(keeps, colnames(RNA.results)), collapse="\n"))
    stop("Terminating program")
}

## Error Checking
db.samples <- dbGetQuery(conn, "SELECT sample_id FROM samples")
db.rna <- dbGetQuery(conn, "SELECT * FROM rna")

validation.results <- tryCatch({
    validateRNA(RNA.results, db.rna=db.rna, sample_ids=db.samples$sample_id)
}, error = function(err) {
    flog.error(err)
    prndlDisconnect()
    flog.error("Terminating program.")
    quit("no")
})

## Sample Info:
## Construct a dataframe of info that may go into the samples table
sampleData <- data.frame(RNA.results$sample_id, RNA.results$study_name)
colnames(sampleData) <- c("sample_id","study_name")
sampleData <- subset(sampleData, sampleData$sample_id %in% validation.results$wrongRNAname)
sampleData <- sampleData[!duplicated(sampleData$sample_id),]

# TODO correct for update
sampleData <- subset(sampleData, !sample_id %in% db.samples$sample_id)
#===============================================================================
# import or update
#===============================================================================

if(nrow(sampleData) > 0){
    InsertQUERY(sampleData, "samples", run = TRUE)
} else{
    flog.info("Nothing to add to samples table")
}

#if (all(sampleData$sample_id %in% db.samples$sample_id)){
#    updateFun.mat(sampleData, "samples", "sample_id", run=TRUE)

#}

# RNA Upload
# Update
if(any(RNA.results$rna_id %in% db.rna$rna_id)) {
    updateFun.mat(subset(RNA.results, rna_id %in% db.rna$rna_id),
                  "rna", "rna_id", run = TRUE)
} else {
    flog.info("Nothing to update")    
}

# Insert New
if(any(!RNA.results$rna_id %in% db.rna$rna_id)) {
    InsertQUERY(subset(RNA.results, !rna_id %in% db.rna$rna_id),
                "rna", run = TRUE)
    flog.info("%s records added", nrow(RNA.results))
} else {
    flog.info("Nothing to add")
}

prndlDisconnect()

#===============================================================================
# END
#===============================================================================
