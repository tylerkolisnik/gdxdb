
#' check water controls
#'
#' check IDs for water controls
#'
#' @param row
#'
#' @return
#'
#' @examples
checkWaterControls <- function(row){
  stopifnot("rna_id" %in% names(row))
  stopifnot("cdna_id" %in% names(row))

  if (row["rna_id"]=="WATER" & grepl("00$", row["cdna_id"])){
    if (!grepl("NE0", row["cdna_id"])){
      return(FALSE)
    }
  }
  return(TRUE)
}

#' validate cDNA
#'
#' Validate cDNA data for DB import.
#'
#' @param results
#' @param rna_ids
#' @param cdna_ids
#'
#' @return
#' @export
#'
#' @examples
validateCDNA <- function(results, rna_ids, cdna_ids){

  if (any(is.na(results$cdna_volume))){
    stop("cDNA volumes are blank. ",
         "They are likely 25 but sometimes mistakes are made and it isn't. ",
         "Contact the lab.")
  }

  sample.id.regex <- "^[A-Z]{2}[0-9]{1}-[0-9]{6}-[0-9]{2}-R[0-9]{2}$"
  if (sum(grepl(sample.id.regex, results$cdna_id)) != nrow(results)){
    stop("One or more cdna_id dooes not match required regex")
  }

  if (!all(apply(results, 1, checkWaterControls))){
    stop("One or more rna_id or cdna_id is invalid for water controls. ",
         "Expecting NE0 prefix for cdna_id if rna_id is WATER")
  }

  if(any(results$nucleic_acid_type != "ssDNA-33")){
    stop("cDNA constant should be 33 for nanodrop results. Check if file is correct.")
  }

  if(sum(duplicated(results$cdna_id)) != 0){
    stop(sprintf("Duplicated cdna_id names:\n%s",
                 paste(results$cdna_id[duplicated(results$cdna_id)],
                       collapse="\n")))
  }

  missingStudy <- subset(results, is.na(study_name))$cdna_id
  if(length(missingStudy) != 0){
    stop(sprintf("cdna is missing study_name: %s", missingStudy))
  }

  repeatcDNAname <- intersect(results$cdna_id, cdna_ids)
  if(length(repeatcDNAname) != 0){
    stop("cdna_id already exists in database - ",
         "cDNA ids must be unique and it is likely that this cDNA is likely a repeat given an identical name. ",
         "The cDNA ID for this sample needs to be modified to be unique. ",
         "After modifying it be sure to modify the cDNA ID in the corresponding MP3 batch to ensure they will map correctly. ",
         sprintf("Repeated cDNA name:\n%s",
                 paste(repeatcDNAname, collapse=",")))
  }

  wrongRNAname <- setdiff(results$rna_id, rna_ids) # needs to be all empty
  if(length(wrongRNAname) != 0){
    stop("rna_id indicated in the cDNA results does not match rna_id records. ",
         "It is likely the rna spreadsheet for this sample has not been uploaded into the database yet. ",
         "MUST BE CORRECTED - modify rna_id column for the following wrong RNA name: ",
         paste(wrongRNAname, collapse=","))
  }

}

#' validate RNA
#'
#' @param results
#' @param db.rna
#' @param sample_ids
#'
#' @return
#' @export
#'
#' @examples
validateRNA <- function(results, db.rna, sample_ids){
  if(!all(colnames(results) %in% colnames(db.rna))){
    stop("Invalid DB columns found in results. ",
         sprintf("Invalid columns:\n%s",
                 setdiff(colnames(results), colnames(db.rna))))
  }

  repeatRNAname <- intersect(results$rna_id, db.rna$rna_id)
  if(length(repeatRNAname) != 0){
    stop("rna_id already exist in database. ",
         "That RNA is likely a repeat and the RNA ID needs to be modified. ",
         "Repeated RNA name: %s",
         paste(repeatRNAname, collapse=","))
  }

  wrongRNAname <- setdiff(results$sample_id, sample_ids)
  if(length(wrongRNAname) != 0){
    warning("sample_id indicated in the RNA results is not in database yet. ",
            "Does any item look like it should already have been in the database? ",
            "For repeats, they should have the same sample_id as the original (ie: sample IDs should not be DEC000-2). ",
            sprintf("wrong RNA name:\n%s", paste(wrongRNAname, collapse="\n")))
  }

  dupeList <- paste0(results$sample_id[duplicated(results$sample_id)], ' ')
  if(!anyDuplicated(results$sample_id)==FALSE){
    stop(sprintf("sample_id indicated in the RNA results is duplicated for samples:\n%s\n",
                 paste(dupeList, collapse=",")),
                 "likely lab did not remove study name on reruns")
  }
  return(list(repeatRNAname=repeatRNAname,
              wrongRNAname=wrongRNAname,
              dupeList=dupeList))

}

#' validate ART
#'
#' @param results
#' @param db.samples.colnames
#'
#' @return
#' @export
#'
#' @examples
validateART <- function(results, db.samples.colnames){

  if(any(!colnames(results) %in% db.samples.colnames)){
    stop(paste0("Column names not found in Samples Table: "),
         paste0(colnames(results)[!colnames(results) %in% db.samples.colnames],';'))
  }

  if(anyDuplicated(results$sample_id) > 0){
    stop("Duplicated Sample IDs not allowed. This must be fixed before proceeding.")
  }
  if(any(is.na(results$sample_id)) == TRUE){
    stop("There is a Sample ID which is NA. this must be fixed before proceeding.")
  }

}
